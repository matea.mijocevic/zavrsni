from django.urls import path
from relations.views import RelationListVieW

urlpatterns = [
      path('relations/', RelationListVieW.as_view(), name="relation-list"),
]
