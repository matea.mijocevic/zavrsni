from django.contrib import admin
from .models import ISCEDArea, Programme, Relation

admin.site.register(ISCEDArea)
admin.site.register(Programme)
admin.site.register(Relation)


# Register your models here.
