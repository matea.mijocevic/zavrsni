from django.db import models
from unis.models import CroUni, ExUni

class ISCEDArea(models.Model):
    isced_id = models.CharField(max_length=100)
    isced_name = models.CharField(max_length=100)

    def __str__(self):
        return self.isced_id

class Programme(models.Model):
    name = models.CharField(max_length=300)
    short_name = models.CharField(max_length=100, default="sms")

    def __str__(self):
        return self.short_name


class Relation(models.Model):
    cro_uni = models.ForeignKey(CroUni, on_delete=models.DO_NOTHING)
    ex_uni = models.ForeignKey(ExUni, on_delete=models.DO_NOTHING)
    ISCED_Area = models.ForeignKey(ISCEDArea, on_delete=models.DO_NOTHING)
    program = models.ForeignKey(Programme, on_delete=models.DO_NOTHING)

    bachelor = models.BooleanField(default=False)
    master = models.BooleanField(default=False)
    doctorate = models.BooleanField(default=False)
    duration_in_months = models.IntegerField()

    def __str__(self):
        return self.cro_uni.name+str("-")+self.ex_uni.name + str("-") + "(" + self.ISCED_Area.isced_id+")"+ self.program.short_name

    class Meta:
        ordering = ('program', 'cro_uni', 'ex_uni', 'duration_in_months')
