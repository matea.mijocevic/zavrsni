from django.shortcuts import render
from django.views.generic import ListView
from .filters import RelationFilter
from .models import Relation


class RelationListVieW(ListView):
    model = Relation
    template_name = 'relations/relations.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['filter'] = RelationFilter(self.request.GET, queryset=self.get_queryset())
        return context


def relation_view(request):
    query = request.GET.get("q", None)
    qs = Relation.objects.all()
    if query is not None:
        qs = qs.filter(program__name__icontains=query)
    context = {
        "object_list": qs
    }
    template = "relations/relations.html"
    return render(request, template, context)
