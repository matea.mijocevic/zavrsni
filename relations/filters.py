import django_filters
from .models import Relation

class RelationFilter(django_filters.FilterSet):

    class Meta:
        model = Relation
        fields = ('cro_uni', 'ex_uni', 'program', 'ISCED_Area', 'bachelor', 'master', 'doctorate', 'duration_in_months')
