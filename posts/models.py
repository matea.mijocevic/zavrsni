from django.db import models
from users.models import CustomUser
from django.urls import reverse
from relations.models import Relation
from unis.models import ExUni
from common.models import City, FinanceType
from PIL import Image
from django.core.validators import MaxValueValidator, MinValueValidator


class Post(models.Model):
    author = models.OneToOneField(CustomUser, on_delete=models.SET_NULL, null=True, blank=True)
    relation = models.ForeignKey(Relation, on_delete=models.SET_NULL, null=True, blank=True)

    title = models.CharField(max_length=100, default="")
    ex_year = models.IntegerField(default=2019, blank=False, validators=[
            MinValueValidator(1975),
            MaxValueValidator(2019)
        ])
    generally = models.TextField(blank=True, null=True)
    last_modified = models.DateTimeField('date_published', auto_now=True)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('post-detail', kwargs={'pk': self.pk})

class AboutCity(models.Model):
    city = models.ForeignKey(City, on_delete=models.CASCADE, null=True)
    post = models.OneToOneField(Post, on_delete=models.CASCADE, null=True)

    places_to_see = models.TextField(blank=True, null=True)
    city_traffic = models.TextField(blank=True, null=True)
    how_to_arrive = models.TextField(blank=True, null=True)
    where_to_go = models.TextField(blank=True, null=True)
    food = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.city.name

    def get_absolute_url(self):
        return reverse('post-detail', kwargs={'pk': self.post.pk})

class AboutUni(models.Model):
    ex_uni = models.ForeignKey(ExUni, on_delete=models.CASCADE, null=True)
    post = models.OneToOneField(Post, on_delete=models.CASCADE, null=True)

    generally = models.TextField(blank=True, null=True)
    prof = models.TextField(blank=True, null=True)
    subjects = models.TextField(blank=True, null=True)

    def get_absolute_url(self):
        return reverse('post-detail', kwargs={'pk': self.post.pk})

    def __str__(self):
        return self.ex_uni.name

class Media(models.Model):
    post = models.ForeignKey(Post, null=True, blank=True, on_delete=models.CASCADE)

    myimage = models.FileField(upload_to='images/', null=True, verbose_name="")

    def __str__(self):
        return str(self.myimage)

    #resize
    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        img = Image.open(self.myimage.path)

        if img.height > 700 or img.width > 700:
            output_size = (700, 700)
            img.thumbnail(output_size)
            img.save(self.myimage.path)



class Finance(models.Model):
    finance_type = models.ForeignKey(FinanceType, on_delete=models.CASCADE)
    post = models.ForeignKey(Post, on_delete=models.CASCADE)
    amount = models.PositiveIntegerField(default=0)
    ordering = ['amount']

    def __str__(self):
        return self.finance_type.__str__() + "-" + self.amount.__str__()

    def get_absolute_url(self):
        return reverse('finance-list', kwargs={'pk': self.post.pk})

    class Meta:
        unique_together = (("finance_type", 'post'),)


