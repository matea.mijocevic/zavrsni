from django.forms import ModelForm
from .models import Post, AboutCity, AboutUni, Media, Finance


class AboutUniForm(ModelForm):
    class Meta:
        model = AboutUni
        fields = ['generally', 'prof', 'subjects', 'ex_uni']


class AboutCityForm(ModelForm):
    class Meta:
        model = AboutCity
        fields = ['city', 'places_to_see', 'city_traffic', 'how_to_arrive', 'where_to_go', 'food']


class PostForm(ModelForm):
    class Meta:
        model = Post
        fields = ['generally', 'title', 'ex_year', 'relation']


class MediaForm(ModelForm):
    class Meta:
        model = Media
        fields = ['myimage']

'''
class FinanceForm(ModelForm):
    class Meta:
        model = Finance
        fields = ['finance_type', 'amount']
'''