from django.urls import path
from . import views
from .views import PostListVieW, PostDetailView, PostUpdateView, PostDeleteView, \
    AboutUniUpdateView, FinanceDeleteView, FinanceUpdateView, \
    AboutCityUpdateView, HomeListVieW, FinanceListVieW, \
    FinanceCreateVieW, MediaListView, MediaDeleteView
from common.views import CityDetailView

urlpatterns = [
    path('', HomeListVieW.as_view(), name="home"),

    path('posts', PostListVieW.as_view(), name="post-list"),
    path('aboutcities/<int:pk>/', CityDetailView.as_view(), name="aboutcity-list"),
    path('finances/<int:pk>/', FinanceListVieW.as_view(), name="finance-list"),

    path('posts/<int:pk>/', PostDetailView.as_view(), name="post-detail"),

    path('posts/<int:pk>/update', PostUpdateView.as_view(), name="post-update"),
    path('posts/aboutuni/<int:pk>/update', AboutUniUpdateView.as_view(), name="aboutuni-update"),
    path('posts/aboutcity/<int:pk>/update', AboutCityUpdateView.as_view(), name="aboutcity-update"),
    path('finances/<int:pk>/update', FinanceUpdateView.as_view(), name="finance-update"),

    path('posts/<int:pk>/delete', PostDeleteView.as_view(), name="post-delete"),
    path('finances/<int:pk>/delete', FinanceDeleteView.as_view(), name="finance-delete"),
    path('images/<int:pk>/delete', MediaDeleteView.as_view(), name="media-delete"),

    path('posts/new/', views.bigpost, name="post-create"),
    path('finances/<int:pk>/new/', FinanceCreateVieW.as_view(), name="finance-create"),

    path('images/<int:pk>/add/', views.saveimage,  name="media-upload"),
    path('images/', MediaListView.as_view(), name="media-list"),

    path('links/', views.links, name="links"),


]
