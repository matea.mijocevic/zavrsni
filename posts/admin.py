from django.contrib import admin
from .models import AboutUni, AboutCity, Post, Media, Finance

admin.site.register(AboutCity)
admin.site.register(AboutUni)
admin.site.register(Post)
admin.site.register(Media)
admin.site.register(Finance)
