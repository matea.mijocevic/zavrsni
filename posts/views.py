from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.views.generic import (ListView, DetailView,	CreateView,	UpdateView,	DeleteView)
from django.contrib import messages
from .models import Post, AboutUni, AboutCity, Finance, Media, FinanceType
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from .forms import AboutCityForm, AboutUniForm, PostForm, MediaForm #, FinanceForm
from users.models import CustomUser
from django.db import IntegrityError
from django.http import HttpResponse




#home
class HomeListVieW(ListView):
    model = Post
    template_name = 'posts/home.html'
    context_object_name = 'posts'
    ordering = ['-last_modified']


#list
class PostListVieW(ListView):
    model = Post
    template_name = 'posts/posts.html'
    context_object_name = 'posts'
    ordering = ['-last_modified']
    paginate_by = 2

class FinanceListVieW(ListView):
    model = Finance
    template_name = 'posts/finance_list.html'
    context_object_name = 'finances'


#update
class PostUpdateView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    model = Post
    fields = ['generally', 'title', 'ex_year', 'relation']

    def form_valid(self, form):
        form.instance.author = self.request.user
        messages.success(self.request, f'Your post has been updated!')
        return super().form_valid(form)

    def test_func(self):
        post = self.get_object()
        if self.request.user == post.author:
            return True
        else:
            return False


class AboutUniUpdateView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    model = AboutUni
    fields = ['generally', 'prof', 'subjects', 'ex_uni']

    def form_valid(self, form):
        form.instance.author = self.request.user
        messages.success(self.request, f'Your post has been updated!')
        return super().form_valid(form)

    def test_func(self):
        aboutuni = self.get_object()
        if self.request.user == aboutuni.post.author:
            return True
        else:
            return False


class AboutCityUpdateView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    model = AboutCity
    fields = ['city', 'places_to_see', 'city_traffic', 'how_to_arrive', 'where_to_go', 'food']

    def form_valid(self, form):
        form.instance.author = self.request.user
        messages.success(self.request, f'Your post has been updated!')
        return super().form_valid(form)

    def test_func(self):
        aboutcity = self.get_object()
        if self.request.user == aboutcity.post.author:
            return True
        else:
            return False


class FinanceUpdateView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    model = Finance
    fields = ['amount']

    def form_valid(self, form):
        form.instance.post.author = self.request.user
        messages.success(self.request, f'Your expense has been updated!')
        return super().form_valid(form)

    def test_func(self):
        finance = self.get_object()
        if self.request.user == finance.post.author:
            return True
        else:
            return False


#detail
class PostDetailView(DetailView):
    model = Post

#delete
class PostDeleteView(LoginRequiredMixin, UserPassesTestMixin, DeleteView):
    model = Post
    success_url = '/'

    def test_func(self):
        post = self.get_object()
        if self.request.user == post.author:
            return True
        else:
            return False


class FinanceDeleteView(LoginRequiredMixin, UserPassesTestMixin, DeleteView):
    model = Finance

    def test_func(self):
        finance = self.get_object()
        if self.request.user == finance.post.author:
            return True
        else :
            return False

    def get_success_url(self):
        return reverse_lazy('finance-list', kwargs={'pk':self.get_object().post.id})


#create
@login_required
def bigpost(request):
    if request.method == 'POST':
        au_form = AboutUniForm(request.POST)
        ac_form = AboutCityForm(request.POST)
        form = PostForm(request.POST)

        if form.is_valid() and ac_form.is_valid() and au_form.is_valid():
            post = form.save(commit=False)
            post.author = request.user
            post.save()
            aboutuni = au_form.save(commit=False)
            aboutcity = ac_form.save(commit=False)

            aboutuni.post = post
            aboutcity.post = post

            aboutcity.save()
            aboutuni.save()

            messages.success(request, f'Your post has been created.')
            return redirect('post-detail', post.id)

    else:
        au_form = AboutUniForm()
        ac_form = AboutCityForm()
        form = PostForm()

    context = {
        'form': form,
        'au_form': au_form,
        'ac_form': ac_form,
    }
    return render(request, 'posts/post_create.html', context)

class FinanceCreateVieW(LoginRequiredMixin, CreateView):
    model = Finance
    fields = ['finance_type', 'amount']
    template_name = 'posts/finance_form.html'

    def form_valid(self, form):
        form.instance.post = self.request.user.post
        try:
            return super().form_valid(form)
        except IntegrityError:
            messages.error(self.request, f'That type of expense already exist. You can edit it.')
            return redirect('finance-list', pk=self.request.user.post.id)


#image

def saveimage(request, pk):
    form = MediaForm(request.POST or None, request.FILES or None)
    if form.is_valid():
        messages.success(request, f'Your picture has been uploaded!')
        media = form.save(commit=False)
        postid = CustomUser.objects.get(pk=pk).post.id
        media.post = Post.objects.get(pk=postid)
        media.save()
    context = {'form': form}
    return render(request, 'posts/images.html', context)


class MediaListView(ListView):
    model = Media
    template_name = 'posts/showimages.html'
    context_object_name = 'images'
    paginate_by = 2

class MediaDeleteView(LoginRequiredMixin, UserPassesTestMixin, DeleteView):
    model = Media

    def test_func(self):
        img = self.get_object()
        if self.request.user == img.post.author:
            return True
        else:
            return False

    def get_success_url(self):
        return reverse_lazy('post-detail', kwargs={'pk':self.get_object().post.id})

def links(request):
    return render(request, 'posts/links.html')
