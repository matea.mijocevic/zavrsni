from django.urls import path
from unis.views import CroUniListVieW, ExUniListVieW, ExUniDetailView, CroUniDetailView, CroUniListVieWSecond

urlpatterns = [
    path('exunis/', ExUniListVieW.as_view(), name="ex-list"),
    path('crounis/', CroUniListVieW.as_view(template_name='unis/crounis1.html'), name="cro-list"),
    path('crounis2/<int:pk>', CroUniListVieWSecond.as_view(template_name='unis/crounis.html'), name="cro-list2"),
    path('exunis/<int:pk>/', ExUniDetailView.as_view(), name="exuni-detail"),
    path('crounis/<int:pk>/', CroUniDetailView.as_view(), name="crouni-detail"),
]
