from django.views.generic import (ListView,	DetailView)
from .models import CroUni, ExUni
from django.db.models.functions import Lower


class ExUniListVieW(ListView):
	model = ExUni
	template_name = 'unis/exunis.html'
	context_object_name = 'exunis'
	paginate_by = 7
	ordering = [Lower('name')]


class ExUniDetailView(DetailView):
	model = ExUni


class CroUniDetailView(DetailView):
	model = CroUni


class CroUniListVieW(ListView):
	model = CroUni
	ordering = [Lower('name')]
	context_object_name = 'crounis'


class CroUniListVieWSecond(ListView):
	model = CroUni
	paginate_by = 7
	ordering = [Lower('name')]

	def get_queryset(self):
		context = CroUni.objects.filter(cro_uni_id = self.kwargs['pk'])
		return context



