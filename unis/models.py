from django.db import models
from common.models import City
from django.urls import reverse


class ExUni(models.Model):
    ex_uni = models.ForeignKey('self', on_delete=models.SET_NULL, null=True, blank=True)
    city = models.ForeignKey(City, on_delete=models.DO_NOTHING, null=True)

    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('exuni-detail', kwargs={'pk': self.pk})

class CroUni(models.Model):
    cro_uni = models.ForeignKey('self', on_delete=models.SET_NULL, null=True, blank=True)

    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('crouni-detail', kwargs={'pk': self.pk})
