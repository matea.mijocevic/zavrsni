from .models import City
from django.views.generic import (ListView, DetailView)
from django.db.models.functions import Lower

class CityListVieW(ListView):
    model = City
    template_name = 'common/cities.html'
    context_object_name = 'cities'
    paginate_by = 7
    ordering = [Lower('country')]

class CityDetailView(DetailView):
    model = City

