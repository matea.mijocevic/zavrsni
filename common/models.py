from django.db import models

from django_countries.fields import CountryField
from django.urls import reverse

class City(models.Model):
    country = CountryField()
    name = models.CharField(max_length=20)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('city-detail', kwargs={'pk': self.pk})

class FinanceType(models.Model):
    type = models.CharField(max_length=30)

    def __str__(self):
        return self.type
