from django.contrib import admin
from .models import City, FinanceType

admin.site.register(City)
admin.site.register(FinanceType)