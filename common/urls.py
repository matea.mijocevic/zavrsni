from django.urls import path
from .views import CityListVieW, CityDetailView

urlpatterns = [
    path('cities/', CityListVieW.as_view(), name="city-list"),
    path('cities/<int:pk>/', CityDetailView.as_view(), name="city-detail")
]


