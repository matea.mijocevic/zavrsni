from django.db import models
from django.contrib.auth.models import AbstractUser
from django.urls import reverse_lazy
from unis.models import CroUni
from . import utils


class CustomUser(AbstractUser):
    university = models.ForeignKey(CroUni, on_delete=models.SET_NULL, null=True)

    email = models.EmailField(('email address'), unique=True)
    email_confirmed = models.BooleanField(default=0)
    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email','first_name', 'last_name']

    def get_absolute_url(self):
        return reverse_lazy('detail', kwargs={'pk': self.pk})


def post_save_reciever(sender,instance,*args,**kwargs):
    if instance.email_confirmed == 0:
        utils.send_mail_to(instance)
    else:
        print('logged in')

#bez ovog se ne salje mail
models.signals.post_save.connect(post_save_reciever,sender=CustomUser)
