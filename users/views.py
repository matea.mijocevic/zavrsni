from django.shortcuts import render, redirect
from django.contrib import messages
from .forms import UserCreationForm, UserUpdateForm
from django.contrib.auth.decorators import login_required
from .forms import UserAuthenticationForm
#other imports
from django.views import generic
from django.urls import reverse_lazy
from django import http
from django.contrib.auth import get_user_model, views as auth_views
from django.contrib.auth.tokens import default_token_generator
from django.utils import http as safehttp
from django.contrib.auth.mixins import LoginRequiredMixin



def checx_inbox(request):
    return render(request, 'users/check_inbox.html')


def register(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            messages.success(request, f"Your account has been created! Please check your mail to activate account.")
            form.save()
            return redirect('check-inbox')
    else:
        form = UserCreationForm()
    return render(request, 'users/register.html', {'form': form})


@login_required
def profile(request):
    if request.method == 'POST':
        u_form = UserUpdateForm(request.POST,request.FILES, instance=request.user)

        if u_form.is_valid():
            u_form.save()
            messages.success(request, f'Your account has been updated.')
            return redirect('profile')

    else:
        u_form = UserUpdateForm(instance=request.user)

    context = {
        'u_form': u_form,
    }
    return render(request, 'users/profile.html', context)

#aut
class UserLoginView(auth_views.LoginView):
    form_class = UserAuthenticationForm
    template_name = 'users/login.html'
    def get_success_url(self, **kwargs):
        return reverse_lazy('home')

class ActivationView(generic.View):
    #Activates user account and redirects to login
    def get(self, request, *args, **kwargs):
        uidb64 = self.kwargs['uidb64']
        token = self.kwargs['token']
        if uidb64 is not None and token is not None:
            uid = safehttp.urlsafe_base64_decode(uidb64)
            user_model = get_user_model()
            user = user_model.objects.get(pk=uid)
            valid = default_token_generator.check_token(user, token)
            try:
                if valid and not request.user.is_authenticated:
                    if user.email_confirmed == 0:
                        user_model.objects.filter(pk=uid).update(email_confirmed = 1)
                        messages.success(self.request, f'You can now log in!')
                        return http.HttpResponseRedirect(reverse_lazy('login'))
                else:
                    return http.HttpResponseRedirect(reverse_lazy('home'))
            except Exception as e:
                print(e)

        return http.HttpResponseRedirect('/')

class CustomPasswordChangeView(LoginRequiredMixin,auth_views.PasswordChangeView):
    def get_success_url(self, **kwargs):
        messages.success(self.request, f'You have sucesfully changed your password.')
        return reverse_lazy('home')
