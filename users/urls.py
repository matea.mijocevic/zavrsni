from django.contrib.auth import views as auth_views
from django.urls import path
from users import views as user_views
from django.conf.urls import url



urlpatterns = [
    path('login/', user_views.UserLoginView.as_view(), name='login'),
    path('logout/', auth_views.LogoutView.as_view(template_name='users/logout.html'), name='logout'),
    path('password-reset/', auth_views.PasswordResetView.as_view(
             template_name='users/password_reset.html'),
         name='password_reset'
         ),
    path('password-reset-complete/',
         auth_views.PasswordResetCompleteView.as_view(
             template_name='users/password_reset_complete.html'),
         name='password_reset_complete'
         ),
    path('password-reset/done/',
         auth_views.PasswordResetDoneView.as_view(
             template_name='users/password_reset_done.html'),
         name='password_reset_done'
         ),
    path('password-reset-confirm/<uidb64>/<token>/',
         auth_views.PasswordResetConfirmView.as_view(
             template_name='users/password_reset_confirm.html'),
         name='password_reset_confirm'
         ),
    #path('register/', user_views.register, name='register'),
    path('register/', user_views.register, name='register'),
    path('profile/', user_views.profile, name='profile'),
    path('register/check/', user_views.checx_inbox, name='check-inbox'),
    url(r'validate/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        user_views.ActivationView.as_view(), name='activate'),

    url(r'password-change/$', user_views.CustomPasswordChangeView.as_view(template_name='users/user_change_password.html'),
        name='user-change-password'),
    url(r'password_change/done/$', auth_views.PasswordChangeDoneView.as_view(template_name='users/user_change_done.html'),
        name='user-change-done'),

]
