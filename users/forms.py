from django import forms
from . import models
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import forms as auth_forms
from django.contrib import messages


class UserCreationForm(auth_forms.UserCreationForm):

    class Meta(auth_forms.UserCreationForm):
        model = models.CustomUser
        fields = ['username','first_name', 'last_name', 'university', 'email', 'password1', 'password2']

    def __init__(self, *args, **kwargs):
        super(UserCreationForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control'


class UserUpdateForm(forms.ModelForm):
    class Meta:
        model = models.CustomUser
        fields = ['username','first_name', 'last_name', 'university', 'email']


#autentification forms
class UserAuthenticationForm(auth_forms.AuthenticationForm):
    def confirm_login_allowed(self, user):
        if not user.is_active:
            messages.error(self.request, f'This account is inactive.')
            raise forms.ValidationError(
                (""),
                code='inactive',
            )
        if not user.email_confirmed:
            messages.error(self.request, f'Sorry, you need to confirm your email.')
            #zbog ovog nam neda login ako nismo potvrdili mail
            raise forms.ValidationError(
                (''),
                code='emial_not_confirmed',
            )
    def __init__(self, *args, **kwargs):
        super(UserAuthenticationForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control'
